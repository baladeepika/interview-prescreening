# PS-palindrome-search exercise

## Instructions

- Using vanilla javascript, create a function that finds the second longest palindrome in a the string
- First Longest Palindrome could be substring for example "referrer". Here referrer is not a palindrome but 'refer' is. So, the second longest palindrome is 'erre'.
- Output of the function should be as follows
  - when no palindrome exists => 'No Palindrome exists'
  - When there is only one palindrome => 'No Second Palindrome exists'
  - When there is a second palindrome => 'Found Palindrome: [PALINDROME]'
- After you complete the exercise, provide any notes on your code below such as how to run your example

## Candidate Notes:

1. Open palindropmeSeach.html
2. In then code you can see the method fnFindPalindrome and  i pass 'refferer' as an argument. In this you can pass any value as an argument.

3. after that open your html file in the browser.
4. Then in the alert box, you can see the output. 